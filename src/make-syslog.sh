#!/bin/bash

. ./config

WINDOW_SIZE=$(($LOG_FETCH_SIZE * $MAX_CONNECTIONS))
LOG_FIFO_SIZE=$(($WINDOW_SIZE * 20))

cat << _EOF_
# This is a generic syslog-ng config using multiple ports and multiple destination
# files in directories named by the port the message arrives. Place a copy of this
# file in the /etc/syslog-ng/conf.d directory and restart syslog-ng. By default, all listeners
# are disabled (commented out), so uncomment the log statements for the ports you will be using.
#
# Also included are pre-defined monitors in inputs.conf that are set to
# monitor each of these directories using the sourcetype=$SPLUNK_SOURCETYPE and
# index=$SPLUNK_INDEX. Adjust the sourcetypes as appropriate for your data.
#
# If the directory names are not to your liking you can modify the "dirname(port_XXXX)"
# in the appropriate destination definition. Do not change the name of the destination,
# only the parameter. If you change it here you will also have to change it in inputs.conf
#
# Also note that certain TAs require specific syslog formats, specifically pan_log and
# bluecoat proxy. There are others, so always check the documentation for the specific
# TA requirements. In most cases using the "t_msg_only" template for that destination
# is sufficient.
#
# Timezones can be specified per-source, so if you have devices that are not logging in UTC
# you can override the timezone for that source port. (e.g. @define port_XXX_tz US/Eastern)
#
# It is recommended that the syslog-ng server itself be set to the UTC timezone.



@define log_root $LOG_ROOT
#       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# ** IMPORTANT ** if you change log_root here you must also adjust
#    inputs.conf
#    props.conf
#    crontab

# The user and group who will own the generated files
@define splunk_user splunk
@define splunk_group splunk

# Generally don't mess with these
@define splunk_udp_driver syslog
@define splunk_tcp_driver network
@define ip_version 4
@define splunk_max_connections $MAX_CONNECTIONS
@define splunk_fetch_limit $LOG_FETCH_SIZE
# make sure splunk_window_size >= splunk_max_connections * splunk_fetch_limit
@define splunk_window_size $WINDOW_SIZE
@define splunk_rcvbuf 425984

# The default TZ for all sources not overridden at the port level
@define default_timezone $DEFAULT_TZ

# DO NOT MODIFY
rewrite r_localhost {
        set("\${FULLHOST_FROM}" value("HOST") condition(host("localhost" flags("ignore-case"))));
        };

# DO NOT MODIFY
template t_standard {
        template("\${DATE} \${HOST} \${MSGHDR}\${MESSAGE}\n");
        };

# DO NOT MODIFY
template t_isodate {
        template("\${ISODATE} \${HOST} \${MSGHDR}\${MESSAGE}\n");
        };

# DO NOT MODIFY
# useful for bluecoat proxy, palo alto, and others
# use only with flags(no-parse)
template t_msg_only {
        template("\${MESSAGE}\n");
        };

# DO NOT MODIFY
block destination d_splunk_logfile( category("") dirname() filename("messages") file_tz(\`default_timezone\`) use_template(t_standard)) {
        file("\`log_root\`/\$(if (\"\`category\`\" == \"\") \"\" \`category\`/)\$(lowercase \"\${LOGHOST}\")/\`dirname\`/\$(lowercase \"\${HOST}\")/\${YEAR}.\${MONTH}.\${DAY}.\${HOUR}-\`filename\`"
                template(\`use_template\`)
                flush-lines(0)
                create-dirs(yes)
                time-zone(\`file_tz\`)
                dir_owner(\`splunk_user\`)
                dir_group(\`splunk_group\`)
                dir_perm(0700)
                owner(\`splunk_user\`)
                group(\`splunk_group\`)
                perm(0600)
                );
        };

# DO NOT MODIFY
block destination d_splunk_high_volume_logfile( category("") dirname() filename("default") file_tz(\`default_timezone\`) use_template(t_standard)) {
        file("\`log_root\`/\$(if (\"\`category\`\" == \"\") \"\" \`category\`/)\$(lowercase \"\${LOGHOST}\")/\`dirname\`/\$(lowercase \"\${HOST}\")/\${YEAR}.\${MONTH}.\${DAY}.\${HOUR}.\${MIN}-\`filename\`"
                template(\`use_template\`)
                flush-lines(0)
                create-dirs(yes)
                time-zone(\`file_tz\`)
                dir_owner(\`splunk_user\`)
                dir_group(\`splunk_group\`)
                dir_perm(0700)
                owner(\`splunk_user\`)
                group(\`splunk_group\`)
                perm(0600)
                );
        };

_EOF_

for i in $(eval echo "$PORT_RANGE");
    do

cat << _EOF_
########################################
#  PORT ${i}
#
# Insure the time-zone is set to the time zone of the *sending* device
#   e.g. @define port_${i}_tz US/Eastern
#
@define port_${i}_tz \`default_timezone\`
#
# The file extension ".syslog" is significant. Files with this extension
# are expected to have ISODATE timestamps and valid syslog formats.
# If you are using a different output template (e.g. t_msg_only) you should
# change the extension to something other than .syslog
#
# Hook commands are only supported in version >= 3.16
#
source s_port_${i} {
        \`splunk_udp_driver\`  ( transport("udp") port(${i})  ip-protocol(\`ip_version\`)
${HOOK_COMMENT}                hook-commands(
${HOOK_COMMENT}                    setup("firewall-cmd --permanent --add-port=${i}/udp")
${HOOK_COMMENT}                    teardown("firewall-cmd --permanent --remove-port=${i}/udp")
${HOOK_COMMENT}                    )
                so-rcvbuf(\`splunk_rcvbuf\`)
                keep-hostname(yes)
                keep-timestamp(yes)
                use-dns(no)
                use-fqdn(no)
                chain-hostnames(off)
                flags(no-parse)
                );

        \`splunk_tcp_driver\`  ( transport("tcp") port(${i})  ip-protocol(\`ip_version\`)
${HOOK_COMMENT}                hook-commands(
${HOOK_COMMENT}                    setup("firewall-cmd --permanent --add-port=${i}/tcp")
${HOOK_COMMENT}                    teardown("firewall-cmd --permanent --remove-port=${i}/tcp")
${HOOK_COMMENT}                    )
                max-connections(\`splunk_max_connections\`)
                log-iw-size(\`splunk_window_size\`)
                log-fetch-limit(\`splunk_fetch_limit\`)
                keep-hostname(yes)
                keep-timestamp(yes)
                use-dns(no)
                use-fqdn(no)
                chain-hostnames(off)
                flags(no-parse)
                );
        };

destination d_port_${i} {

        # normal logging creates a new file every hour
        d_splunk_logfile(

        # use the line below instead if your device generates more than 100MB/min
        # creates a new file every minute
        # d_splunk_high_volume_logfile(

            dirname(port_${i})
            category(${DEFAULT_CATEGORY})
            # filename(\${FACILITY}.\${LEVEL})
            filename(generic.syslog)
            file_tz(\`port_${i}_tz\`)
            use_template(t_isodate));
        };

 # This is for use where the Splunk TA does not expect syslog-formatted messages (i.e. bluecoat and pan:log)
 destination d_port_${i}_msg_only {

        # normal logging creates a new file every hour
        d_splunk_logfile(

        # use the line below instead if your device generates more than 100MB/min
        # creates a new file every minute
        # d_splunk_high_volume_logfile(

            dirname(port_${i})
            category(${DEFAULT_CATEGORY})
            filename(msg_only.log)
            file_tz(\`port_${i}_tz\`)
            use_template(t_msg_only));
        };

# This is "normal" RFC3164-formatted (BSD-syslog) messages.
# if you want to change the timezone, do it in the @define statement above for consistency
${LOG_COMMENT}log { source(s_port_${i}); parser { syslog-parser(time-zone(\`port_${i}_tz\`)); }; rewrite(r_localhost); destination(d_port_${i}); flags(final); };

# For RFC5424-formatted messages, use this instead (your messages show <##>1 at the beginning)
# if you want to change the timezone, do it in the @define statement above for consistency
#log { source(s_port_${i}); parser { syslog-parser(time-zone(\`port_${i}_tz\`) flags(syslog-protocol)); }; rewrite(r_localhost); destination(d_port_${i}); flags(final); };

# Always try this one first if unsure.
# For palo alto and Bluecoat ProxySG devices, or other devices where the TA specifies "message only" use this statement
# the "parser" directive is purposely left out so the entire message will be contained in the \${MESSAGE} macro
# timezone adjustment is not possible without using a parser, so props.conf will be needed.
#log { source(s_port_${i}); destination(d_port_${i}_msg_only); flags(final); };

# A variation with no-parse flag that might work better
#log { source(s_port_${i}); { syslog-parser(time-zone(\`port_${i}_tz\`) flags(no-parse)); } destination(d_port_${i}_msg_only); flags(final); };

_EOF_

    done

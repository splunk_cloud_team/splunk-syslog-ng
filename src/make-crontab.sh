#!/bin/bash
. ./config

cat << _EOF_
# per minute files have 4 dots before the dash ${YEAR}.${MONTH}.${DAY}.${HOUR}.${MIN}
# every minute compress files not modified in over 5 minutes
* * * * * $FIND $LOG_ROOT -type f -name "*.*.*.*.*-*" -not -name "*.gz" -mmin +5 -exec $GZIP {} \;

# per hour files have 3 dots before the dash ${YEAR}.${MONTH}.${DAY}.${HOUR}
# at 5 minutes past every hour compress files not modified in over 1 hour
5 * * * * $FIND $LOG_ROOT -type f -name "*.*.*.*-*" -not -name "*.*.*.*.*-*" -not -name "*.gz" -mmin +60 -exec $GZIP {} \;

# at 10 minutes past every hour delete any files not modified in more than 24 hours
10 * * * * $FIND $LOG_ROOT -type f -mmin +1440 -delete

# delete empty directories every night at midnight
0 0 * * * $FIND $LOG_ROOT/* -type d -empty -delete

_EOF_
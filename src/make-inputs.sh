#!/bin/bash

. ./config

if [ -z ${DEFAULT_CATEGORY} ]; then
	SEGMENTS=3
else
	CATEGORY=/${DEFAULT_CATEGORY}
	SEGMENTS=4
fi

for i in $(eval echo "$PORT_RANGE")
	do
		echo "########################################"
		echo "#  PORT ${i}"
		echo "#"
		echo "[monitor://${LOG_ROOT}${CATEGORY}/.../port_$i/.../*.(sys)?log(\.gz)?]"
		echo "index = ${SPLUNK_INDEX}"
		echo "sourcetype = ${SPLUNK_SOURCETYPE}"
		echo "host_segment = $(( $(grep -o / <<<${LOG_ROOT} | wc -l) + ${SEGMENTS}))"
		echo "crcSalt = $i"
		echo "disabled = 1"
		echo ""
		echo ""
	done

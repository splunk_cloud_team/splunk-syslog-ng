#!/bin/bash

. ./config

for i in $(eval echo "$PORT_RANGE")
    do
        echo "firewall-cmd --permanent --add-port=$i/tcp"
        echo "firewall-cmd --permanent --add-port=$i/udp"
    done
echo "firewall-cmd --reload"
echo "firewall-cmd --list-ports"

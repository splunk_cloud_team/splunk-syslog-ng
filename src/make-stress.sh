#!/bin/bash

. ./config

echo "for i in $(eval echo \"$PORT_RANGE\")"
echo "	do"
echo "		loggen -i --size 3000 --rate 1000 --interval 1 127.0.0.1 \$i & > /dev/null 2>&1 "
echo "	done"

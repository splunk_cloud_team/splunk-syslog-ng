#!/bin/bash

. ./config

cat << _EOF_
# This file is auto-generated when this TA is first created based on the value of \${LOG_ROOT}
# if you update log_root in /etc/syslog-ng/conf.d/splunk.conf then you must update the path here
# don't forget to update the cloud indexers and heavy forwarders if appropriate.

[source::${LOG_ROOT}/.../*.log]
EVENT_BREAKER_ENABLE = 1
EVENT_BREAKER = ([\n\r]+)
SHOULD_LINEMERGE = 0
LINE_BREAKER = ([\n\r]+)
MAX_TIMESTAMP_LOOKAHEAD = 30
TIME_PREFIX = ^

[source::${LOG_ROOT}/.../*.log.gz]
EVENT_BREAKER_ENABLE = 1
EVENT_BREAKER = ([\n\r]+)
SHOULD_LINEMERGE = 0
LINE_BREAKER = ([\n\r]+)
MAX_TIMESTAMP_LOOKAHEAD = 30
TIME_PREFIX = ^

[source::${LOG_ROOT}/.../*.syslog]
EVENT_BREAKER_ENABLE = 1
EVENT_BREAKER = ([\n\r]+)
SHOULD_LINEMERGE = 0
LINE_BREAKER = ([\n\r]+)
TIME_FORMAT = %FT%T%z
MAX_TIMESTAMP_LOOKAHEAD = 30
TIME_PREFIX = ^

[source::${LOG_ROOT}/.../*.syslog.gz]
EVENT_BREAKER_ENABLE = 1
EVENT_BREAKER = ([\n\r]+)
SHOULD_LINEMERGE = 0
LINE_BREAKER = ([\n\r]+)
TIME_FORMAT = %FT%T%z
MAX_TIMESTAMP_LOOKAHEAD = 30
TIME_PREFIX = ^

[source::${LOG_ROOT}/.../*.syslog_ml]
EVENT_BREAKER_ENABLE = 1
EVENT_BREAKER = ([\n\r]+)(?:(?:\d{4}-\d{2}-\d{2}[T ]\d{2}:\d{2}:\d{2}(?:[,.]\d+)?(?:[+-]\d{2}:\d{2})?))
SHOULD_LINEMERGE = 0
LINE_BREAKER = ([\n\r]+)(?:(?:\d{4}-\d{2}-\d{2}[T ]\d{2}:\d{2}:\d{2}(?:[,.]\d+)?(?:[+-]\d{2}:\d{2})?))
MAX_TIMESTAMP_LOOKAHEAD = 30
TIME_FORMAT = %FT%T%z
TIME_PREFIX = ^

[source::${LOG_ROOT}/.../*.syslog_ml.gz]
EVENT_BREAKER_ENABLE = 1
EVENT_BREAKER = ([\n\r]+)(?:(?:\d{4}-\d{2}-\d{2}[T ]\d{2}:\d{2}:\d{2}(?:[,.]\d+)?(?:[+-]\d{2}:\d{2})?))
SHOULD_LINEMERGE = 0
LINE_BREAKER = ([\n\r]+)(?:(?:\d{4}-\d{2}-\d{2}[T ]\d{2}:\d{2}:\d{2}(?:[,.]\d+)?(?:[+-]\d{2}:\d{2})?))
MAX_TIMESTAMP_LOOKAHEAD = 30
TIME_FORMAT = %FT%T%z
TIME_PREFIX = ^

_EOF_

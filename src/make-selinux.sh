#!/bin/bash

. ./config

for i in $(eval echo "$PORT_RANGE")
	do
		echo "semanage port -a -t syslogd_port_t -p tcp $i"
		echo "semanage port -a -t syslogd_port_t -p udp $i"
	done
echo "semanage port --list | grep syslog"

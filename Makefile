BUILDDIR := ./build
DISTDIR := ./archive
SRCDIR := ./src

HF_NAME := TA-syslog-ng
HF_DEFAULT := $(BUILDDIR)/$(HF_NAME)/default
HF_DEFAULTS := $(addprefix $(HF_DEFAULT)/,props.conf app.conf)

TA_NAME := TA-syslog-ng_inputs
TA_DEFAULT := $(BUILDDIR)/$(TA_NAME)/default
TA_LOCAL := $(BUILDDIR)/$(TA_NAME)/local
TA_EXTRA := $(BUILDDIR)/$(TA_NAME)/extra

DEFAULTS := $(addprefix $(TA_DEFAULT)/,limits.conf server.conf props.conf app.conf)
EXTRAS := $(addprefix $(TA_EXTRA)/,splunk.conf crontab update-firewall.sh update-selinux.sh stress-me.sh timezones.list)
LOCALS := $(addprefix $(TA_LOCAL)/,inputs.conf)

START_PORT := 20500
END_PORT := 20599
PORT_RANGE := 514 {20500..20599}
LOG_ROOT=/opt/syslog-ng/logs
SPLUNK_INDEX=syslog
SPLUNK_SOURCETYPE=syslog
LOG_FETCH_SIZE=100
MAX_CONNECTIONS=1000
FIND=/usr/bin/find
GZIP=/usr/bin/gzip

all: $(DEFAULTS) $(EXTRAS) $(LOCALS) $(HF_DEFAULTS)
	@echo ""; echo "==> $@ built."; echo ""
	@echo "Make any required modifications in $(BUILDDIR)/$(TA_NAME)"
	@echo "Specifically, update sourcetype and index if needed in $(BUILDDIR)/$(TA_NAME)/local/inputs.conf"
	@echo "and make any syslog-ng changes in $(TA_EXTRA)/splunk.conf."
	@echo "Finally, run 'make dist' to create the installation archive."
	@echo ""

configure:
	@clear
	@echo ""
	@echo "Enter the ports you want syslog-ng to listen on"
	@echo "You can specify individual ports, ranges, or combinations"
	@echo "For a single port just enter the port number. (e.g. 514)"
	@echo "For a range of ports use curly braces. (e.g. {1514..1524})"
	@echo "To use combinations, separate the components with spaces. (e.g. 514 {1514..1524} 1526)"
	@echo "If you need a large range, the the ports {20500..20599} are unassigned."
	@echo ""; read -p "==> PORT_RANGE [ $(PORT_RANGE) ]: " read_var; echo PORT_RANGE=\"$${read_var:-'$(PORT_RANGE)'}\" > ./config
	@echo ""; echo "Enter the path to the log files on the syslog-server"
	@echo ""; read -p "==> LOG_ROOT [ $(LOG_ROOT) ]: " read_var; echo LOG_ROOT=$${read_var:-$(LOG_ROOT)} >> ./config
	@echo ""; echo "Enter the default index"
	@echo ""; read -p "==> SPLUNK_INDEX [ $(SPLUNK_INDEX) ]: " read_var; echo SPLUNK_INDEX=$${read_var:-$(SPLUNK_INDEX)} >> ./config
	@echo ""; echo "Enter the default sourcetype"
	@echo ""; read -p "==> SPLUNK_SOURCETYPE [ $(SPLUNK_SOURCETYPE) ]: " read_var; echo SPLUNK_SOURCETYPE=$${read_var:-$(SPLUNK_SOURCETYPE)} >> ./config

	@echo ""; echo "Enter the path to the 'find' command"
	@echo ""; read -p "==> FIND [ $(FIND) ]: " read_var; echo FIND=$${read_var:-$(FIND)} >> ./config

	@echo ""; echo "Enter the path to the 'gzip' command"
	@echo ""; read -p "==> GZIP [ $(GZIP) ]: " read_var; echo GZIP=$${read_var:-$(GZIP)} >> ./config

	@echo LOG_FETCH_SIZE=$(LOG_FETCH_SIZE) >> config
	@echo MAX_CONNECTIONS=$(MAX_CONNECTIONS) >> config
	# @echo FIND=$(FIND) >> config
	# @echo GZIP=$(GZIP) >> config
	@echo "# To enable a default category (e.g. pci) specify it here" >> config
	@echo DEFAULT_CATEGORY= >> config
	@echo "# To enable all log statements delete the # in the following line" >> config
	@echo LOG_COMMENT=# >> config
	@echo "# To enable the hook-commands delete the # in the following line" >> config
	@echo HOOK_COMMENT=# >> config
	@echo ""; echo "==> Configuration built"; echo ""
	@echo "Verify settings in 'config'"
	@echo "run 'make all' to build the source from the settings in 'config'"
	@echo ""


$(TA_EXTRA)/splunk.conf : $(SRCDIR)/make-syslog.sh  config
	@echo "==> Building syslog-ng configuration file"
	@/bin/bash $< > $@

$(TA_LOCAL)/inputs.conf : $(SRCDIR)/make-inputs.sh  config
	@echo "==> Building inputs.conf configuration file"
	@/bin/bash $< > $@

$(TA_DEFAULT)/props.conf : $(SRCDIR)/make-props.sh  config
	@echo "==> Copying props.conf configuration file"
	@/bin/bash $< > $@

$(TA_DEFAULT)/limits.conf : $(SRCDIR)/$(TA_NAME)/default/limits.conf
	@echo "==> Copying limits.conf configuration file"
	@cp -r $< $@

$(TA_DEFAULT)/server.conf : $(SRCDIR)/$(TA_NAME)/default/server.conf
	@echo "==> Copying server.conf configuration file"
	@cp -r $< $@

$(TA_DEFAULT)/app.conf : $(SRCDIR)/$(TA_NAME)/default/app.conf_inputs
	@echo "==> Copying app.conf configuration file"
	@cp -r $< $@

$(TA_EXTRA)/crontab : $(SRCDIR)/make-crontab.sh  config
	@echo "==> Building crontab file"
	@/bin/bash $< > $@

$(TA_EXTRA)/update-firewall.sh : $(SRCDIR)/make-firewall.sh  config
	@echo "==> Building firewall rules script"
	@/bin/bash $< > $@

$(TA_EXTRA)/update-selinux.sh : $(SRCDIR)/make-selinux.sh  config
	@echo "==> Building SELinux rules script"
	@/bin/bash $< > $@

$(TA_EXTRA)/stress-me.sh : $(SRCDIR)/make-stress.sh  config
	@echo "==> Building stress testing script"
	@/bin/bash $< > $@

$(TA_EXTRA)/timezones.list : $(SRCDIR)/timezones.list  config
	@echo "==> Copying timezone list"
	@cp -r $< $@

$(HF_DEFAULT)/props.conf : $(SRCDIR)/make-props.sh  config
	@echo "==> Copying props.conf configuration file"
	@/bin/bash $< > $@

$(HF_DEFAULT)/app.conf : $(SRCDIR)/$(TA_NAME)/default/app.conf
	@echo "==> Copying app.conf configuration file"
	@cp -r $< $@

$(DEFAULTS): | $(TA_DEFAULT)
$(LOCALS): | $(TA_LOCAL)
$(EXTRAS): | $(TA_EXTRA)
$(HF_DEFAULTS): | $(HF_DEFAULT)

$(BUILDDIR):
	@echo "==> creating $@ directory"
	@mkdir $@

$(BUILDDIR)/$(TA_NAME): | $(BUILDDIR)
	@echo "==> creating $@ directory"
	@mkdir $@

$(BUILDDIR)/$(HF_NAME): | $(BUILDDIR)
	@echo "==> creating $@ directory"
	@mkdir $@

$(HF_DEFAULT): | $(BUILDDIR)/$(HF_NAME)
	@echo "==> creating $@ directory"
	@mkdir $@

$(TA_DEFAULT): | $(BUILDDIR)/$(TA_NAME)
	@echo "==> creating $@ directory"
	@mkdir $@

$(TA_EXTRA): | $(BUILDDIR)/$(TA_NAME)
	@echo "==> creating $@ directory"
	@mkdir $@

$(TA_LOCAL): | $(BUILDDIR)/$(TA_NAME)
	@echo "==> creating $@ directory"
	@mkdir $@

.PHONY: clean
clean:
	rm -rf $(BUILDDIR) ./config

dist-clean:
	rm -rf $(BUILDDIR) $(DISTDIR) ./config .dist

$(DISTDIR):
	@echo "==> creating $@ directory"
	@mkdir $@

dist: $(DEFAULTS) $(EXTRAS) $(LOCALS) $(HF_DEFAULTS) | $(DISTDIR)
	@echo "==> creating archive files"
	@tar -czf $(DISTDIR)/$(TA_NAME).tgz -C $(BUILDDIR) $(TA_NAME)
	@tar -czf $(DISTDIR)/$(HF_NAME).tgz -C $(BUILDDIR) $(HF_NAME)

